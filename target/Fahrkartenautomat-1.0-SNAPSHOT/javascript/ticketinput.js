let buttons = document.getElementsByTagName('button');
for (let index = 0; index < buttons.length; index++) {
    let button = buttons[index];

    if (button.dataset.id === undefined) {
        console.log("button with id " + button.id + " failed to find ticket, cause \"eingabe\" is not provided!");
        continue;
    }

    button.addEventListener('click', function () {
        location.href="/ticket?id=" + button.dataset.id;
    });
}