
$(document).ready(function() {
    $('.coin').on('click', async function() {
     await $.ajax({
            url: '/getMoneyValue',
            type: 'POST',
            data: {coin: $(this).data("value")},
            success : function(responseText) {
                let $output = $('#restbetrag');

                let zahlenderBetrag = $output.text();
                let topay = zahlenderBetrag - responseText;
                let topayInt = topay.valueOf().toFixed(2);

                if (topayInt <= 0) {
                    let change = 0 - topayInt;
                    $('input[name="exchange"]').val(change);
                    $("#payment").submit();
                }

                $output.text(topayInt);
            },
            error: function (error) {
                console.log("error occurred", error);
                alert(error.responseText);
            }
        });

    });
});