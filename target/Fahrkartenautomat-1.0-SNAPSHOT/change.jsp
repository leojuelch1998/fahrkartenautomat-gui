<%@ page import="com.example.fahrkartenautomat.dto.Auszahlen" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%ArrayList auszahlen = (ArrayList)request.getAttribute("auszahlen");  %>
<%--
  Created by IntelliJ IDEA.
  User: Leonardo
  Date: 08.02.2022
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Rueckgeld</title>
    <script src="javascript/jquery-1.10.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/change.css">
</head>
<body>

<div class="project-container">
    <div class="screen">
        <div class="helper-container">
            <div class="warten-text">Rückgeld wird ausgegeben</div>
            <div class="loadbox">
                <div id="load-text"></div>
            </div>
            <div id="change">
                <% for (Object o : auszahlen) { %>
                <% Auszahlen aus = (Auszahlen) o; %>

                    <% if (aus.getPreis() == 2.0) { %>
                    <div class="coin-pictures disappear-coin">
                        <img src="pictures/2euro.svg" alt="2€">
                    </div>
                    <%  } %>
                    <% if (aus.getPreis() == 1.0) { %>
                    <div class="coin-pictures disappear-coin">
                        <img src="pictures/1euro.svg" alt="1€">
                    </div>
                    <%  } %>
                    <% if (aus.getPreis() == 0.5) { %>
                    <div class="coin-pictures disappear-coin">
                        <img src="pictures/50ct.svg" alt="50cent">
                    </div>
                    <%  } %>
                    <% if (aus.getPreis() == 0.2) { %>
                    <div class="coin-pictures disappear-coin">
                        <img src="pictures/20ct.svg" alt="20cent">
                    </div>
                    <%  } %>
                    <% if (aus.getPreis() == 0.1) { %>
                    <div class="coin-pictures disappear-coin">
                        <img src="pictures/10ct.svg" alt="10cent">
                    </div>
                    <%  } %>
                    <% if (aus.getPreis() == 0.05) { %>
                    <div class="coin-pictures disappear-coin">
                        <img src="pictures/5ct.svg" alt="5cent">
                    </div>
                    <%  } %>
                <% } %>
            </div>
        </div>

        <div id="bottom-buttons">
            <div class="left-bottom">
                <div class="bottom-btn-content">
                    <button id="entnehmen" class="btn-bottom-deactivated">Münzen entnehmen</button>
                </div>
            </div>
            <div class="right-bottom">
                <div class="bottom-btn-content">
                    <button id="beenden" class="btn-bottom-deactivated">Beenden</button>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="javascript/change.js"></script>
</body>
</html>







