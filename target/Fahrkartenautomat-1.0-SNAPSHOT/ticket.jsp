<%@ page import="com.example.fahrkartenautomat.dto.Ticket" %>

<%--
  Created by IntelliJ IDEA.
  User: Leonardo
  Date: 01.02.2022
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Fahrkartenautomat</title>
    <link rel="stylesheet" href="css/ticket.css">
    <script src="javascript/jquery-1.10.2.min.js" type="text/javascript"></script>
</head>

<body>

<div class="project-container">
    <div class="screen">

        <div class="bezahlen-selection">
            <div class="bezahlen-selection-left">
                <div class="ticket-section">
                    <div class="inner-ticket-section"><%= ((Ticket) request.getAttribute("ticket")).getBezeichnung() %></div>
                </div>
                <div class="tarif-section">
                    <div class="inner-tarif-section">
                        <div class="inner-tarif-section1">1</div>
                        <div class="inner-tarif-section2">Regeltarif</div>
                    </div>
                </div>
                <div class="betrag-preis-section">
                    <div class="inner-betrag-preis-section">
                        <div class="lef-text">Gesamtbetrag</div>
                        <div class="right-preis">EUR <%= ((Ticket) request.getAttribute("ticket")).getPreis() %>0</div>
                    </div>
                </div>
            </div>
            <div class="bezahlen-selection-right">
                <button class="btn" id="regeltarif">Regeltarif</button>
                <button class="btn" id="ermaessigt">Ermäßigungstarif</button>
                <button class="btn" id="mehrere">mehrere Fahrscheine</button>
            </div>
        </div>

        <div class="noch-zu-zahlen">
            <div class="text-muenzen">
                <div class="text1">Noch zu zahlender Betrag</div>
                <div class="text2">zahlbar mit:</div>
                <div class="muenzen">
                    <form id="payment" action="ausgeben" method="post">
                        <input type="hidden" name="exchange" value="0">
                        <img class="coin" src="pictures/5ct.svg" data-value="0.05" alt="5 cent">
                        <img class="coin" src="pictures/10ct.svg" data-value="0.10" alt="10 cent">
                        <img class="coin" src="pictures/20ct.svg" data-value="0.20" alt="20 cent">
                        <img class="coin" src="pictures/50ct.svg" data-value="0.50" alt="50 cent">
                        <img class="coin" src="pictures/1euro.svg" data-value="1.00" alt="1 euro">
                        <img class="coin" src="pictures/2euro.svg" data-value="2.00" alt="2 euro">
                    </form>
                </div>
            </div>
            <div class="offener-restbetrag">
                <div class="restbetrag-content">EUR
                    <span id="restbetrag"><%=((Ticket) request.getAttribute("ticket")).getPreis()%></span>
                </div>
            </div>
        </div>

        <div class="sidebar">
            <div class="sidebar-content">
                <div id="bvg-logo">
                    <div id="helper1">BVG</div>
                </div>
                <div class="back-arrow">
                    <div class="yellow-circle">
                        <a href="loadingscreen2.jsp" id="back-icon"><input type="image" src="pictures/arrow-left-solid.svg" alt="arrow"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="javascript/fixNull.js"></script>
<script src="javascript/pay-ajax.js"></script>
<script src="javascript/getexchange.js"></script>

</body>

</html>

