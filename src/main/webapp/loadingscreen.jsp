<%--
  Created by IntelliJ IDEA.
  User: Leonardo
  Date: 01.02.2022
  Time: 14:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Gute Fahrt</title>
    <link rel="stylesheet" href="css/guteFahrt.css">
</head>

<body>

    <div class="project-container">
        <div class="screen">
            <div class="helper-container">
                <div class="warten-text">Danke für ihren Einkauf. <br> Wir wünschen ihnen eine gute Fahrt!</div>
                <div class="loadbox"></div>
            </div>
        </div>
    </div>

    <script src="javascript/redirect.js"></script>
</body>

</html>
