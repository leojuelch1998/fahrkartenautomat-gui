<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 01.02.2022
  Time: 23:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Fahrkartenautomat</title>
    <link rel="stylesheet" href="css/loadingscreen.css">
</head>

<body>

<div class="project-container">
    <div class="screen">
        <div class="helper-container">
            <div class="warten-text">Bitte Warten...</div>
            <div class="loadbox"></div>
        </div>
    </div>
</div>

<script src="javascript/redirectBack.js"></script>
</body>

</html>