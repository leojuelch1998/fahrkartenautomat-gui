<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Fahrkartenautomat</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

<div class="project-container">
    <div class="screen">

        <div id="header">
            <h1>Wählen sie den gewünschten Fahrschein</h1>
        </div>

        <div class="ticket-container">
            <div class="center-content">
                <button class="btn" id="kurzstrecke" data-id="1">Kurzstrecke </button>
            </div>
            <div class="right-content"></div>
        </div>

        <div class="ticket-container">
            <div class="left-content">
                <div class="streckencontent">
                    <div class="center-inner-content">
                        <div class="berlin-line">-------- Berlin --------</div>
                        <div class="abc">
                            <div class="abc-left">A</div>
                            <div class="abc-center">B</div>
                            <div class="abc-right" id="noC"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center-content">
                <button class="btn" id="ab-einzelfahrschein" data-id="2">Einzelfahrschein</button>
            </div>
            <div class="right-content">
                <button class="btn" id="ab-tageskarte" data-id="5">Tageskarte</button>
            </div>
        </div>

        <div class="ticket-container">
            <div class="left-content">
                <div class="streckencontent">
                    <div class="center-inner-content">
                        <div class="berlin-line">-------- Berlin --------</div>
                        <div class="abc">
                            <div class="abc-left" id="noA"></div>
                            <div class="abc-center">B</div>
                            <div class="abc-right">C</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center-content">
                <button class="btn" id="bc-einzelfahrschein" data-id="3">Einzelfahrschein</button>
            </div>
            <div class="right-content">
                <button class="btn" id="bc-tageskarte" data-id="6">Tageskarte</button>
            </div>

        </div>
        <div class="ticket-container">
            <div class="left-content">
                <div class="streckencontent">
                    <div class="center-inner-content">
                        <div class="berlin-line">-------- Berlin --------</div>
                        <div class="abc">
                            <div class="abc-left">A</div>
                            <div class="abc-center">B</div>
                            <div class="abc-right">C</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="center-content">
                <button class="btn" id="abc-einzelfahrschein" data-id="4">Einzelfahrschein</button>
            </div>
            <div class="right-content">
                <button class="btn" id="abc-tageskarte" data-id="7">Tageskarte</button>
            </div>
        </div>

        <div id="bottom-buttons">
            <div class="left-bottom">
                <div class="bottom-btn-content">
                    <button class="btn-bottom" id="andere-fahrscheine" data-id="11">Andere Fahrscheine...</button>
                </div>
            </div>
            <div class="right-bottom">
                <div class="bottom-btn-content">
                    <button class="btn-bottom" id="handy-aufladen" data-id="12">Handy aufladen...</button>
                </div>
            </div>
        </div>

        <div class="sidebar">
            <div id="bvg-logo">
                <div id="helper1">BVG</div>
            </div>
            <div id="vbb-ticket"></div>
            <div class="back-arrow"></div>
        </div>

    </div>
</div>

<script src="javascript/ticketinput.js"></script>

</body>

</html>
