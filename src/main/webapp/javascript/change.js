

setTimeout(function () {

    let coins = $('.coin-pictures');

    coins.removeClass("disappear-coin");
    coins.addClass("appear-coin");

    $("#entnehmen, #beenden").removeClass();
    $("#entnehmen, #beenden").addClass("btn-bottom");

    $(".loadbox").css("background-color", "#f0d723");
    $("#load-text").text("Vorgang abgeschlossen, bitte Münzen entnehmen");

    $('#beenden').click(function () {
        window.location = "loadingscreen.jsp";
    });
    $('#entnehmen').click(function () {
        $('.coin-pictures').animateAppendTo('#entnehmen', 1000);
        $('.warten-text').text("Rückgeld wurde ausgegeben");
        $('#load-text').text("Sie können ihren Einkauf nun beenden");
        $("#entnehmen").removeClass();
        $("#entnehmen").addClass("btn-bottom-deactivated");
        setTimeout(function (){
           window.location = "loadingscreen.jsp";
        }, 15000);
    });

}, 6000);



$.fn.animateAppendTo = function(sel, speed) {
    let $this = this,
        newEle = $this.clone(true).appendTo(sel),
        newPos = newEle.position();
    newEle.hide();
    $this.css('position', 'absolute').animate(newPos, speed, function() {
        $this.remove();
    });
    return newEle;
};
