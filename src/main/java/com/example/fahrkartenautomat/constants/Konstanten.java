package com.example.fahrkartenautomat.constants;
import com.example.fahrkartenautomat.dto.Auszahlen;
import com.example.fahrkartenautomat.dto.Ticket;

import java.util.Arrays;
import java.util.List;

public class Konstanten {

    public static List<Ticket> TICKETS = Arrays.asList(
        new Ticket(1, "Kurzstrecke", 1.90),
        new Ticket(2, "Einzelfahrschein AB", 2.90),
        new Ticket(3, "Einzelfahrschein BC", 3.30),
        new Ticket(4, "Einzelfahrschein ABC", 3.60),
        new Ticket(5, "Tageskarte Berlin AB", 8.60),
        new Ticket(6, "Tageskarte Berlin BC", 9.00),
        new Ticket(7, "Tageskarte Berlin ABC", 9.60),
        new Ticket(8, "Kleingruppe Tageskarte Berlin AB", 23.50),
        new Ticket(9, "Kleingruppe Tageskarte Berlin BC", 24.30),
        new Ticket(10, "Kleingruppe Tageskarte Berlin ABC", 24.90)
    );

    public static List<Auszahlen> AUSZAHLEN = Arrays.asList(
        new Auszahlen("2 €", 2.00),
        new Auszahlen("1 €", 1.00),
        new Auszahlen("50 ct", 0.50),
        new Auszahlen("20 ct", 0.20),
        new Auszahlen("10 ct", 0.10),
        new Auszahlen("5 ct", 0.05)
    );
}
