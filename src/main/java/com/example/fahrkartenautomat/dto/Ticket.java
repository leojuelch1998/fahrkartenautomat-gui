package com.example.fahrkartenautomat.dto;

public class Ticket {

    private final int id;
    private final String bezeichnung;
    private final double preis;

    public Ticket (int eingabe, String bezeichnung, double preis) {
        this.id = eingabe;
        this.bezeichnung = bezeichnung;
        this.preis = preis;
    }

    public int getId() {return id;}

    public double getPreis() {
        return preis;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }
}
