package com.example.fahrkartenautomat.dto;

public class Auszahlen {

    private final String bezeichnung;
    private final double preis;

    public Auszahlen (String bezeichnung, double preis) {
        this.bezeichnung = bezeichnung;
        this.preis = preis;
    }

    public String getBezeichnung(){return bezeichnung;}

    public double getPreis(){return preis;}
}
