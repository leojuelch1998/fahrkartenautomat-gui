package com.example.fahrkartenautomat.dto;

public class Auswahlmoeglichkeiten {

    private String bezeichnung;
    private int eingabe;

    public Auswahlmoeglichkeiten (String bezeichnung, int eingabe) {
        this.bezeichnung = bezeichnung;
        this.eingabe = eingabe;
    }

    public String getBezeichnung() {return bezeichnung;}

    public int getEingabe() {return eingabe;}
}
