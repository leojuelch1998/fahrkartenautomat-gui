package com.example.fahrkartenautomat.controller;

import com.example.fahrkartenautomat.dto.Auszahlen;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Optional;

import static com.example.fahrkartenautomat.constants.Konstanten.AUSZAHLEN;


@WebServlet(value = "/getMoneyValue")
public class payController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        double coin = Optional.ofNullable(request.getParameter("coin"))
                .map(Double::parseDouble)
                .orElse(0.0);

        boolean muenzeAkzeptiert = AUSZAHLEN.stream()
                .map(Auszahlen::getPreis)
                .anyMatch(preis -> preis == coin);

        if (!muenzeAkzeptiert) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().write("Münze nicht erkannt");
            return;
        }

//        if (isAutomatScheisse()) {
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            response.getWriter().write("Pissa, fick auf dein Münze erst zur Reinigung bringen");
//            return;
//        }

        response.getWriter().write(request.getParameter("coin"));
    }

    private boolean isAutomatScheisse() {
        return Math.random() * 100 <= 10;
    }
}
