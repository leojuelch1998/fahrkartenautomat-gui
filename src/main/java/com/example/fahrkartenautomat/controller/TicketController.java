package com.example.fahrkartenautomat.controller;

import com.example.fahrkartenautomat.dto.Ticket;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;

import static com.example.fahrkartenautomat.constants.Konstanten.TICKETS;

@WebServlet(value = "/ticket")
public class TicketController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String eingabe = request.getParameter("id");

        for (Ticket TICKET : TICKETS) {
            if (String.valueOf(TICKET.getId()).equals(eingabe)) {
                request.setAttribute("ticket", TICKET);
                break;
            }
        }

        request.getRequestDispatcher("/ticket.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
