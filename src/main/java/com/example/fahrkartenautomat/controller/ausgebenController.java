package com.example.fahrkartenautomat.controller;

import com.example.fahrkartenautomat.dto.Auszahlen;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.*;

import static com.example.fahrkartenautomat.constants.Konstanten.AUSZAHLEN;

@WebServlet(value = "/ausgeben")
public class ausgebenController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        double exchange = Optional.ofNullable(request.getParameter("exchange")).map(Double::parseDouble).orElse(0.0);

        List<Auszahlen> returnCoins = new ArrayList<>();
        while (exchange > 0.0){
            exchange = payoutCoin(exchange, returnCoins);
        }

        request.setAttribute("auszahlen", returnCoins);
        request.getRequestDispatcher("change.jsp").forward(request, response);
    }
    private double payoutCoin(double exchange, List<Auszahlen> returnCoins) {
        for (Auszahlen auszahlen : AUSZAHLEN) {
            if (exchange < auszahlen.getPreis()) {
                continue;
            }
            returnCoins.add(auszahlen);
            exchange -= auszahlen.getPreis();
            return Optional.of(exchange)
                    .map(this::formatToValue)
                    .map(Formatter::toString)
                    .map(Double::parseDouble)
                    .orElse(0.0);
        }
        throw new IllegalStateException("failed to payback, please restart");
    }

    private Formatter formatToValue(Double change) {
        return new Formatter(Locale.US).format("%.2f", change);
    }

}

